<?php
//helper dd
function dd($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
    die();
}
//Check if user is logged in
function IsLogged()
{
    if (isset($_SESSION['user'])) {
        return true;
    } else {
        return false;
    }
}
//Check if user is admin
function IsAdmin()
{
    if (isset($_SESSION['user']) && $_SESSION['user']->admin == 1) {
        return true;
    } else {
        return false;
    }
}
//Validate email
function ValidateEmail($email)
{
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    } else {
        return false;
    }
}
//Validate password
function ValidatePassword($password)
{
    if (strlen($password) >= 6) {
        return true;
    } else {
        return false;
    }
}
//Validate NIF
function ValidateNIF($nif)
{
    if (preg_match('/^[0-9]{8}[A-Z]$/', $nif)) {
        return true;
    } else {
        return false;
    }
}
//Redirect
function redirect($url)
{
    header('Location: ' . URLROOT . '/' . $url);
    exit();
}

//Get brand name from id
function getBrandName($brand_id)
{
    $car = new Car();
    $brand = $car->getBrand($brand_id);
    return $brand->Name;
}

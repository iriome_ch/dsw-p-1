<?php
if (!session_id()) {
    session_start();
}

use \Dotenv\Dotenv;

use \Tamtamchik\SimpleFlash\Flash;


require_once 'vendor/autoload.php';
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

require_once 'config/config.php';
require_once 'exceptions/FileException.php';
require_once 'helpers/utils.php';



spl_autoload_register(function ($class) {
    require_once 'lib/' . $class . '.php';
});



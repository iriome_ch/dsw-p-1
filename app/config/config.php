<?php
//SIMPLE CONFIGURATION
define('APPROOT', dirname(dirname(__FILE__)));

if(isset($_SERVER['HTTPS'])) :
define('URLROOT', 'HTTPS://iriomech.es');
else :
define('URLROOT', 'HTTP://iriomech.es');
endif;

define('SITENAME', 'Framework');


//DATABASE CONFIGURATION
define('DB_HOST', $_ENV['DB_HOST']);
define('DB_USER', $_ENV['DB_USER']);
define('DB_PASS', $_ENV['DB_PASS']);
define('DB_NAME', $_ENV['DB_NAME']);

<?php
class Rent
{

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getRents()
    {
        $this->db->query('SELECT * FROM Rent');
        return $this->db->resultSet();
    }

    public function getRent($user_NIF, $Cars_Plate, $Rent_Date)
    {
        $this->db->query('SELECT * FROM Rent WHERE users_NIF = :user_NIF AND Cars_Plate = :Cars_Plate AND Rent_Date = :Rent_Date');
        $this->db->bind(':user_NIF', $user_NIF);
        $this->db->bind(':Cars_Plate', $Cars_Plate);
        $this->db->bind(':Rent_Date', $Rent_Date);
        $row = $this->db->single();
        return $row;
    }

    public function addRent($data)
    {
        $this->db->query('INSERT INTO Rent (users_NIF, Cars_Plate,Rent_Date) VALUES (:user_NIF, :Cars_Plate, :Rent_Date)');
        // Bind values
        $this->db->bind(':user_NIF', $data['users_NIF']);
        $this->db->bind(':Cars_Plate', $data['Cars_Plate']);
        $this->db->bind(':Rent_Date', $data['Rent_Date']);
        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function updateRent($data)
    {
        $this->db->query('UPDATE Rent SET Return_Date= :Return_Date WHERE users_NIF = :users_NIF AND Cars_Plate = :Cars_Plate');
        // Bind values
        $this->db->bind(':users_NIF', $data['Users_NIF']);
        $this->db->bind(':Cars_Plate', $data['Cars_Plate']);
        $this->db->bind(':Return_Date', Date('Y-m-d H:i:s'));
        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteRent($user_NIF, $Cars_Plate, $Rent_Date)
    {
        $this->db->query('DELETE FROM Rent WHERE user_NIF = :user_NIF AND Cars_Plate = :Cars_Plate AND Rent_Date = :Rent_Date');
        // Bind values
        $this->db->bind(':user_NIF', $user_NIF);
        $this->db->bind(':Cars_Plate', $Cars_Plate);
        $this->db->bind(':Rent_Date', $Rent_Date);
        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getRentsByUser($user_NIF)
    {
        $this->db->query('SELECT * FROM Rent WHERE user_NIF = :user_NIF');
        $this->db->bind(':user_NIF', $user_NIF);
        $row = $this->db->resultSet();
        return $row;
    }

    public function getRentsByCar($Cars_Plate)
    {
        $this->db->query('SELECT * FROM Rent WHERE Cars_Plate = :Cars_Plate');
        $this->db->bind(':Cars_Plate', $Cars_Plate);
        $row = $this->db->resultSet();
        return $row;
    }

    public function getRentsByDate($Rent_Date)
    {
        $this->db->query('SELECT * FROM Rent WHERE Rent_Date = :Rent_Date');
        $this->db->bind(':Rent_Date', $Rent_Date);
        $row = $this->db->resultSet();
        return $row;
    }
    
    public function getRentByUserAndCar($user_NIF, $Cars_Plate)
    {
        $this->db->query('SELECT * FROM Rent WHERE user_NIF = :user_NIF AND Cars_Plate = :Cars_Plate');
        $this->db->bind(':user_NIF', $user_NIF);
        $this->db->bind(':Cars_Plate', $Cars_Plate);
        $row = $this->db->single();
        return $row;
    }

    public function findRentByCar($Cars_Plate)
    {
        $this->db->query('SELECT * FROM Rent WHERE Cars_Plate = :Cars_Plate');
        $this->db->bind(':Cars_Plate', $Cars_Plate);
        if($this->db->single()){
            return true;
        }else{
            return false;
        }
    }

    public function findRentByUser($user_NIF)
    {
        $this->db->query('SELECT * FROM Rent WHERE users_NIF = :user_NIF');
        $this->db->bind(':user_NIF', $user_NIF);
        if($this->db->single()){
            return true;
        }else{
            return false;
        }
    }

}

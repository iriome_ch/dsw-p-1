<?php
class Car
{


    public function __construct()
    {
        $this->db = new Database();
    }

    public function getAllCars()
    {
        $this->db->query('SELECT * FROM Cars ORDER BY name DESC');
        return $this->db->resultSet();
    }

    public function getCar($Plate)
    {
        $this->db->query('SELECT * FROM Cars where Plate = :Plate');
        $this->db->bind(':Plate', $Plate);
        return $this->db->single();
    }

    public function addCar($data)
    {
        $this->db->query('INSERT INTO Cars (Plate, Brands_idBrands, name, Years, color, price, description,  image, status,luggage, fuel, seats ,Features, mileage, transmission) VALUES (:Plate, :Brands_idBrands, :name, :Years, :color, :price, :description, :image, :status, :luggage, :fuel, :seats, :Features, :mileage, :transmission)');
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':price', $data['price']);
        $this->db->bind(':description', $data['description']);
        $this->db->bind(':image', $data['image']);
        $this->db->bind(':status', $data['status']);
        $this->db->bind(':luggage', $data['luggage']);
        $this->db->bind(':fuel', $data['fuel']);
        $this->db->bind(':seats', $data['seats']);
        $this->db->bind(':Features', $data['Features']);
        $this->db->bind(':mileage', $data['mileage']);
        $this->db->bind(':transmission', $data['transmission']);
        $this->db->bind(':Brands_idBrands', $data['Brand_idBrands']);
        $this->db->bind(':Plate', $data['Plate']);
        $this->db->bind(':Years', $data['Years']);
        $this->db->bind(':color', $data['color']);
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function updateCar($data)
    {
        $this->db->query('UPDATE Cars SET name = :name, price = :price, description = :description, image = :image, status = :status,luggage = :luggage, fuel = :fuel, seats = :seats, mileage = :mileage, transmission = :transmission, Brands_idBrands = :Brands_idBrands, Years = :Years, color = :color WHERE Plate = :Plate');
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':price', $data['price']);
        $this->db->bind(':description', $data['description']);
        $this->db->bind(':image', $data['image']);
        $this->db->bind(':status', $data['status']);
        $this->db->bind(':luggage', $data['luggage']);
        $this->db->bind(':fuel', $data['fuel']);
        $this->db->bind(':seats', $data['seats']);
        $this->db->bind(':mileage', $data['mileage']);
        $this->db->bind(':transmission', $data['transmission']);
        $this->db->bind(':Brands_idBrands', $data['Brand_idBrands']);
        $this->db->bind(':Plate', $data['Plate']);
        $this->db->bind(':Years', $data['Years']);
        $this->db->bind(':color', $data['color']);
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteCar($Plate)
    {
        $this->db->query('DELETE FROM Cars WHERE Plate = :Plate');
        $this->db->bind(':Plate', $Plate);
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllBrands()
    {
        $this->db->query('SELECT * FROM Brands');
        return $this->db->resultSet();
    }

    public function getBrand($id)
    {
        $this->db->query('SELECT * FROM Brands WHERE idBrands = :id');
        $this->db->bind(':id', $id);
        return $this->db->single();
    }

    public function addBrand($data)
    {
        $this->db->query('INSERT INTO Brands (name) VALUES (:name)');
        $this->db->bind(':name', $data['name']);
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function updateBrand($data)
    {
        $this->db->query('UPDATE Brands SET name = :name WHERE idBrands = :id');
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':id', $data['id']);
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteBrand($id)
    {
        $this->db->query('DELETE FROM Brands WHERE idBrands = :id');
        $this->db->bind(':id', $id);
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getManyFirstCars($number)
    {
        $this->db->query('SELECT * FROM Cars inner join Brands on Cars.Brands_idBrands = Brands.idBrands LIMIT :number');
        $this->db->bind(':number', $number);
        return $this->db->resultSet();
    }

    public function findCarByPlate($Plate)
    {
        $this->db->query('SELECT * FROM Cars WHERE Plate = :Plate');
        $this->db->bind(':Plate', $Plate);
        if ($row = $this->db->single()) {
            return $row;
        } else {
            return false;
        }
    }

    public function updateCarStatus($Plate, $status)
    {
        $this->db->query('UPDATE Cars SET status = :status WHERE Plate = :Plate');
        $this->db->bind(':status', $status);
        $this->db->bind(':Plate', $Plate);
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }

    }
}

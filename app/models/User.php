<?php
class User
{
    public function __construct()
    {
        $this->db = new Database();
    }

    public function register($data)
    {
        $this->db->query('INSERT INTO Users (name,last_name , email, password, NIF, address, phone,admin, City,State,Country,zip ) VALUES(:name,:last_name,:email,:password,:NIF,:address,:phone,:admin,:City,:State,:Country,:zip)');
        // Bind values
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':last_name', $data['lastname']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);
        $this->db->bind(':NIF', $data['NIF']);
        $this->db->bind(':address', $data['address']);
        $this->db->bind(':phone', $data['phone']);
        $this->db->bind(':admin', $data['admin']);
        $this->db->bind(':City', $data['city']);
        $this->db->bind(':State', $data['state']);
        $this->db->bind(':Country', $data['country']);
        $this->db->bind(':zip', $data['ZIP']);
        // Execute
        
        if ($row = $this->db->execute()) {
            return $row;
        } else {
            return false;
        }
    }

    public function login($email, $password, $NIF)
    {
        $this->db->query('SELECT * FROM Users WHERE email = :email AND NIF = :NIF');
        $this->db->bind(':email', $email);
        $this->db->bind(':NIF', $NIF);

        $row = $this->db->single();

        $hashed_password = $row->password;
        if ($password == $hashed_password) {
            return $row;
        } else {
            return false;
        }
    }

    public function findUserByEmail($email)
    {
        $this->db->query('SELECT * FROM Users WHERE email = :email');
        // Bind value
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        // Check row
        if ($this->db->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserByNIF($NIF)
    {
        $this->db->query('SELECT * FROM Users WHERE NIF = :NIF');
        // Bind value
        $this->db->bind(':NIF', $NIF);

        $row = $this->db->single();

        // Check row
        if ($this->db->rowCount() > 0) {
            return $row;
        } else {
            return false;
        }
    }

    public function getAllUsers()
    {
        $this->db->query('SELECT * FROM Users');
        $results = $this->db->resultSet();
        return $results;
    }

    public function updateUser($data)
    {
        $this->db->query('UPDATE Users SET name = :name, last_name = :last_name, address = :address, phone = :phone, City = :City, State = :State, Country = :Country, zip = :zip WHERE NIF = :NIF');
        // Bind values
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':last_name', $data['lastname']);
        $this->db->bind(':NIF', $data['NIF']);
        $this->db->bind(':address', $data['address']);
        $this->db->bind(':phone', $data['phone']);
        $this->db->bind(':City', $data['city']);
        $this->db->bind(':State', $data['state']);
        $this->db->bind(':Country', $data['country']);
        $this->db->bind(':zip', $data['ZIP']);
        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteUser($NIF)
    {
        $this->db->query('DELETE FROM Users WHERE NIF = :NIF');
        // Bind value
        $this->db->bind(':NIF', $NIF);
        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getManyFirstsUsers($number)
    {
        $this->db->query('SELECT * FROM Users LIMIT :number');
        // Bind value
        $this->db->bind(':number', $number);

        $results = $this->db->resultSet();
        return $results;
    }

    public function countUsers()
    {
        $this->db->query('SELECT COUNT(*) FROM Users');
        $results = $this->db->single();
        return $results;
    }

    public function deleteAllUsers()
    {
        $this->db->query('DELETE FROM Users where admin = 0');
        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function setadmin($NIF)
    {
        $this->db->query('UPDATE Users SET admin = 1 WHERE NIF = :NIF');
        // Bind values
        $this->db->bind(':NIF', $NIF);
        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
}

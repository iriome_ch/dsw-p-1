<?php include_once APPROOT . "/views/partials/header.php"; ?>
<div class="container ">
    <div class="row m-5">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
                <h2>Confirm Rent</h2>
                <p>Please confirm your rent details</p>
                <form action="<?php echo URLROOT; ?>/rents/make" method="post">
                    <div class="form-group">
                        <input type="hidden" name="Cars_Plate" value="<?php echo $data['Cars_Plate']; ?>">
                        <input type="submit" class="btn btn-success btn-block" name="confirm" value="Confirm">
                    </div>
                    <div class="form-group">
                        <a href="<?php echo URLROOT; ?>/paginas/cars" class="btn btn-light">
                            <i class="fa fa-backward"></i> Back
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<?php include_once APPROOT . "/views/partials/footer.php"; ?>
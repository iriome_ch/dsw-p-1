<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="<?= URLROOT ?>/">Car<span>Renting</span>House</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="<?= URLROOT ?>/" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="<?= URLROOT ?>/paginas/about" class="nav-link">About</a></li>
                <li class="nav-item"><a href="<?= URLROOT ?>/paginas/cars" class="nav-link">Cars</a></li>
            </ul>
        </div>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <?php if (IsLogged() && IsAdmin()) : ?>
                    <li class="nav-item"><a href="<?= URLROOT ?>/admin/index" class="nav-link">Admin</a></li>
                    <li class="nav-item"><a href="<?= URLROOT ?>/users/logout" class="nav-link">Logout</a></li>
                <?php elseif (IsLogged()) : ?>
                  <li class="nav-item"><a href="<?= URLROOT ?>/users/logout" class="nav-link">Logout</a></li>
                <?php else : ?>
                    <li class="nav-item"><a href="<?= URLROOT ?>/users/registerUser" class="nav-link">Register</a></li>
                    <li class="nav-item"><a href="<?= URLROOT ?>/users/loginUser" class="nav-link">Login</a></li>
                <?php endif; ?>
            </ul>
        </div>
</nav>
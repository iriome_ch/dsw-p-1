<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $data['title'] ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?= URLROOT ?>/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?= URLROOT ?>/css/animate.css">

    <link rel="stylesheet" href="<?= URLROOT ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= URLROOT ?>/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?= URLROOT ?>/css/magnific-popup.css">

    <link rel="stylesheet" href="<?= URLROOT ?>/css/aos.css">

    <link rel="stylesheet" href="<?= URLROOT ?>/css/ionicons.min.css">

    <link rel="stylesheet" href="<?= URLROOT ?>/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?= URLROOT ?>/css/jquery.timepicker.css">


    <link rel="stylesheet" href="<?= URLROOT ?>/css/flaticon.css">
    <link rel="stylesheet" href="<?= URLROOT ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?= URLROOT ?>/css/style.css">
</head>

<body>
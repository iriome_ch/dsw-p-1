<?php include_once APPROOT . '/views/partials/adminheader.php' ?>



<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
                <h2>Add Brand</h2>
                <p>Please fill this form to add a brand</p>
                <form action="<?php echo URLROOT; ?>/cars/addBrand" method="post">
                    <div class="form-group">
                        <label for="name">Brand Name: <sup>*</sup></label>
                        <input type="text" name="name" class="form-control form-control-lg <?php echo (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['name']; ?>">
                        <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="submit" value="Add Brand" class="btn btn-success btn-block">
                        </div>
                        <div class="col">
                            <a href="<?php echo URLROOT; ?>/admin/index" class="btn btn-light btn-block">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include_once APPROOT . '/views/partials/adminfooter.php' ?>
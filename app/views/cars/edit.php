<?php include_once APPROOT . '/views/partials/adminheader.php' ?>

<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
                <h2>Edit Car</h2>
                <p>Please fill out this form to edit a car</p>
                <form action="<?php echo URLROOT; ?>/cars/edit" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">Car Name: <sup>*</sup></label>
                        <input type="text" name="name" class="form-control form-control-lg <?php echo (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['name']; ?>">
                        <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="price">Price: <sup>*</sup></label>
                        <input type="text" name="price" class="form-control form-control-lg <?php echo (!empty($data['price_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['price']; ?>">
                        <span class="invalid-feedback"><?php echo $data['price_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="description">Description: <sup>*</sup></label>
                        <textarea name="description" class="form-control form-control-lg <?php echo (!empty($data['description_err'])) ? 'is-invalid' : ''; ?>"><?php echo $data['description']; ?></textarea>
                        <span class="invalid-feedback"><?php echo $data['description_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="image">Image: <sup>*</sup></label>
                        <input type="file" name="image" class="form-control form-control-lg <?php echo (!empty($data['image_err'])) ? 'is-invalid' : ''; ?>">
                        <span class="invalid-feedback"><?php echo $data['image_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="status">Status: <sup>*</sup></label>
                        <select name="status" class="form-control form-control-lg <?php echo (!empty($data['status_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Car Status</option>
                            
                            <option value="Available" <?php echo ($data['status'] == 'Available') ? 'selected' : ''; ?>>Available</option>
                            <option value="Borrowed" <?php echo ($data['status'] == 'Borrowed') ? 'selected' : ''; ?>>Borrowed</option>
                            <option value="Lost" <?php echo ($data['status'] == 'Lost') ? 'selected' : ''; ?>>Lost</option>
                            <option value="Damaged" <?php echo ($data['status'] == 'Damaged') ? 'selected' : ''; ?>>Damaged</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['status_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="luggage">Luggage: <sup>*</sup></label>
                        <select name="luggage" class="form-control form-control-lg <?php echo (!empty($data['luggage_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Car Luggage</option>
                            <option value="small" <?php echo ($data['luggage'] == 'small') ? 'selected' : ''; ?> >Small</option>
                            <option value="medium" <?php echo ($data['luggage'] == 'medium') ? 'selected' : ''; ?>>Medium</option>
                            <option value="longer" <?php echo ($data['luggage'] == 'longer') ? 'selected' : ''; ?>>Large</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['luggage_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="fuel">Fuel: <sup>*</sup></label>
                        <select name="fuel" class="form-control form-control-lg <?php echo (!empty($data['fuel_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Fuel Type</option>
                            <option value="Gasolina" <?php echo ($data['fuel'] == 'Gasolina') ? 'selected' : ''; ?>>Petrol</option>
                            <option value="Diesel" <?php echo ($data['fuel'] == 'Diesel') ? 'selected' : ''; ?>>Diesel</option>
                            <option value="Eléctrico" <?php echo ($data['fuel'] == 'Eléctrico') ? 'selected' : ''; ?>>Electric</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['fuel_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="transmission">Transmission: <sup>*</sup></label>
                        <select name="transmission" class="form-control form-control-lg <?php echo (!empty($data['transmission_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Transmission Type</option>
                            <option value="manual" <?php echo ($data['transmission'] == 'manual') ? 'selected' : ''; ?>>Manual</option>
                            <option value="auto" <?php echo ($data['transmission'] == 'auto') ? 'selected' : ''; ?>>Automatic</option>
                            <option value="doble-clutch" <?php echo ($data['transmission'] == 'doble-clutch') ? 'selected' : ''; ?>>Doble-clutch</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['transmission_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="seats">Seats: <sup>*</sup></label>
                        <select name="seats" class="form-control form-control-lg <?php echo (!empty($data['seats_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Number of Seats</option>
                            <option value="2" <?php echo ($data['seats'] == '2') ? 'selected' : ''; ?>>2</option>
                            <option value="4" <?php echo ($data['seats'] == '4') ? 'selected' : ''; ?>>4</option>
                            <option value="5" <?php echo ($data['seats'] == '5') ? 'selected' : ''; ?>>5</option>
                            <option value="7" <?php echo ($data['seats'] == '7') ? 'selected' : ''; ?>>7</option>
                            <option value="8" <?php echo ($data['seats'] == '8') ? 'selected' : ''; ?>>8</option>
                            <option value="9" <?php echo ($data['seats'] == '9') ? 'selected' : ''; ?>>9</option>
                            <option value="10" <?php echo ($data['seats'] == '10') ? 'selected' : ''; ?>>10</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['seats_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="Years">Years: <sup>*</sup></label>
                        <input type="number" name="Years" class="form-control form-control-lg <?php echo (!empty($data['Years_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Years']; ?>">
                        <span class="invalid-feedback"><?php echo $data['Years_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="mileage">Mileage: <sup>*</sup></label>
                        <input type="number" name="mileage" class="form-control form-control-lg <?php echo (!empty($data['mileage_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['mileage']; ?>">
                        <span class="invalid-feedback"><?php echo $data['mileage_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="color">Color: <sup>*</sup></label>
                        <input type="text" name="color" class="form-control form-control-lg <?php echo (!empty($data['color_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['color']; ?>">
                        <span class="invalid-feedback"><?php echo $data['color_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="Brand">Brand: <sup>*</sup></label>
                        <select name="Brand_idBrands" class="form-control form-control-lg <?php echo (!empty($data['Brand_idBrands_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Car Brand</option>
                            <?php foreach($data['Brands'] as $brand): ?>
                                <option value="<?php echo $brand->idBrands; ?>"><?php echo $brand->Name; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['Brand_idBrands_err']; ?></span>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="hidden" name="Plate" value="<?php echo $data['Plate']; ?>">
                            <input type="submit" value="Submit" class="btn btn-success btn-block">
                        </div>
                        <div class="col">
                            <a href="<?php echo URLROOT; ?>/admin/index" class="btn btn-light btn-block">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include_once APPROOT . '/views/partials/adminfooter.php' ?>
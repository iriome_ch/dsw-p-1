<?php include_once APPROOT . '/views/partials/adminheader.php' ?>
</div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card card-body bg-light mt-5">
                    <h2>Delete Car</h2>
                    <p>Are you sure you want to delete this car?</p>
                    <form action="<?php echo URLROOT; ?>/cars/delete" method="post">
                        <div class="row">
                            <div class="col">
                                <input type="hidden" name="Plate" value="<?= $data['Plate'] ?>">
                                <input type="submit" value="Yes" class="btn btn-danger btn-block">
                            </div>
                            <div class="col">
                                <a href="<?php echo URLROOT; ?>/admin/index" class="btn btn-success btn-block">No</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php include_once APPROOT . '/views/partials/adminfooter.php' ?>
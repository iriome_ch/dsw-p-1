<?php include_once APPROOT . '/views/partials/adminheader.php' ?>

<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
                <h2>Add Car</h2>
                <p>Please fill out this form to add a car</p>
                <form action="<?php echo URLROOT; ?>/cars/add" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">Car Name: <sup>*</sup></label>
                        <input type="text" name="name" class="form-control form-control-lg <?php echo (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['name']; ?>">
                        <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="price">Price: <sup>*</sup></label>
                        <input type="text" name="price" class="form-control form-control-lg <?php echo (!empty($data['price_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['price']; ?>">
                        <span class="invalid-feedback"><?php echo $data['price_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="description">Description: <sup>*</sup></label>
                        <textarea name="description" class="form-control form-control-lg <?php echo (!empty($data['description_err'])) ? 'is-invalid' : ''; ?>"><?php echo $data['description']; ?></textarea>
                        <span class="invalid-feedback"><?php echo $data['description_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="image">Image: <sup>*</sup></label>
                        <input type="file" name="image" class="form-control form-control-lg <?php echo (!empty($data['image_err'])) ? 'is-invalid' : ''; ?>">
                        <span class="invalid-feedback"><?php echo $data['image_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="status">Status: <sup>*</sup></label>
                        <select name="status" class="form-control form-control-lg <?php echo (!empty($data['status_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Car Status</option>
                            <option value="Available">Available</option>
                            <option value="Borrowed">Borrowed</option>
                            <option value="Lost">Lost</option>
                            <option value="Damaged">Damaged</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['status_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="luggage">Luggage: <sup>*</sup></label>
                        <select name="luggage" class="form-control form-control-lg <?php echo (!empty($data['luggage_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Car Luggage</option>
                            <option value="small">Small</option>
                            <option value="medium">Medium</option>
                            <option value="longer">Large</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['luggage_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="fuel">Fuel: <sup>*</sup></label>
                        <select name="fuel" class="form-control form-control-lg <?php echo (!empty($data['fuel_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Fuel Type</option>
                            <option value="Gasolina">Petrol</option>
                            <option value="Diesel">Diesel</option>
                            <option value="Eléctrico">Electric</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['fuel_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="transmission">Transmission: <sup>*</sup></label>
                        <select name="transmission" class="form-control form-control-lg <?php echo (!empty($data['transmission_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Transmission Type</option>
                            <option value="manual">Manual</option>
                            <option value="auto">Automatic</option>
                            <option value="doble-clutch">Doble-clutch</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['transmission_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="seats">Seats: <sup>*</sup></label>
                        <select name="seats" class="form-control form-control-lg <?php echo (!empty($data['seats_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Number of Seats</option>
                            <option value="2">2</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['seats_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="Years">Years: <sup>*</sup></label>
                        <input type="number" name="Years" class="form-control form-control-lg <?php echo (!empty($data['Years_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Years']; ?>">
                        <span class="invalid-feedback"><?php echo $data['Years_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="mileage">Mileage: <sup>*</sup></label>
                        <input type="number" name="mileage" class="form-control form-control-lg <?php echo (!empty($data['mileage_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['mileage']; ?>">
                        <span class="invalid-feedback"><?php echo $data['mileage_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="color">Color: <sup>*</sup></label>
                        <input type="text" name="color" class="form-control form-control-lg <?php echo (!empty($data['color_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['color']; ?>">
                        <span class="invalid-feedback"><?php echo $data['color_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="Plate">Plate: <sup>*</sup></label>
                        <input type="text" name="Plate" class="form-control form-control-lg <?php echo (!empty($data['Plate_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Plate']; ?>">
                        <span class="invalid-feedback"><?php echo $data['Plate_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="Brand">Brand: <sup>*</sup></label>
                        <select name="Brand_idBrands" class="form-control form-control-lg <?php echo (!empty($data['brand_err'])) ? 'is-invalid' : ''; ?>">
                            <option value="">Select Car Brand</option>
                            <?php foreach($data['Brands'] as $brand): ?>
                                <option value="<?php echo $brand->idBrands; ?>"><?php echo $brand->Name; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['brand_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="features">Features: <sup>*</sup></label>
                        <select name="features" class="form-control form-control-lg <?php echo (!empty($data['features_err'])) ? 'is-invalid' : ''; ?>" multiple size="5" id = "features">
                            <option value="">Select Car Features</option>
                            <option value="air-conditioning">Air Conditioning</option>
                            <option value="abs">ABS</option>
                            <option value="air-bag">Air Bag</option>
                            <option value="alarm">Alarm</option>
                            <option value="antilock-brakes">Antilock Brakes</option>
                            <option value="bluetooth">Bluetooth</option>
                            <option value="cd-player">CD Player</option>
                            <option value="cruise-control">Cruise Control</option>
                            <option value="esp-spoiler">ESP Spoiler</option>
                            <option value="heated-seats">Heated Seats</option>
                            <option value="leather-seats">Leather Seats</option>
                            <option value="navigation-system">Navigation System</option>
                            <option value="rear-camera">Rear Camera</option>
                            <option value="rear-view-camera">Rear View Camera</option>
                            <option value="rear-window-defroster">Rear Window Defroster</option>
                            <option value="rear-window-wiper">Rear Window Wiper</option>
                            <option value="remote-start">Remote Start</option>
                            <option value="satellite-radio">Satellite Radio</option>
                            <option value="sensor-deceleration">Sensor Deceleration</option>
                            <option value="sensor-parking">Sensor Parking</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['features_err']; ?></span>
                    </div>

                    <div class="row">
                        <div class="col">
                            <input type="submit" value="Submit" class="btn btn-success btn-block">
                        </div>
                        <div class="col">
                            <a href="<?php echo URLROOT; ?>/admin/index" class="btn btn-light btn-block">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include_once APPROOT . '/views/partials/adminfooter.php' ?>
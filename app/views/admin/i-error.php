<?php include_once APPROOT . '/views/partials/adminheader.php' ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card card-body bg-light mt-5">
                    <h2>Integrity Error</h2>
                    <p>You can´t delete this record because it is referenced by other records.</p>
                    <a href="<?php echo URLROOT; ?>/admin/index" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
    </div>

<?php include_once APPROOT . '/views/partials/adminfooter.php' ?>
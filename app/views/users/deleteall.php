<?php include_once APPROOT."/views/partials/adminheader.php" ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Delete All Users</h1>
            <p>Are you sure you want to delete all users?</p>
            <form action="<?= URLROOT ?>/users/deleteAllUsers" method="post">
                <input type="submit" value="Delete All Users" class="btn btn-danger">
            </form>
            <a href="<?= URLROOT ?>/admin/index" class="btn btn-secondary">Cancel</a>
        </div>
    </div>

<?php include_once APPROOT . '/views/partials/adminfooter.php' ?>
<?php include_once APPROOT . '/views/partials/adminheader.php' ?>

    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card card-body bg-light mt-5">
                    <h2>Set Admin</h2>
                    <p>Set the admin account</p>
                    <form action="<?php echo URLROOT; ?>/users/setadminUser" method="post">
                        <div class="form-group">
                            <input type="hidden" name="NIF" value="<?php echo $data['NIF']; ?>">
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="submit" value="Yes" class="btn btn-success btn-block">
                            </div>
                            <div class="col">
                                <a href="<?php echo URLROOT; ?>/admin/index" class="btn btn-light btn-block">No</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php include_once APPROOT . '/views/partials/adminfooter.php' ?>
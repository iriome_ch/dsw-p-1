<?php include_once APPROOT . '/views/partials/adminheader.php' ?>

<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
                <h2>Add User</h2>
                <p>Please fill this form to add a user</p>
                <form action="<?php echo URLROOT; ?>/users/updateUser" method="post">
                    <div class="form-group">
                        <label for="name">Name: <sup>*</sup></label>
                        <input type="text" name="name" class="form-control form-control-lg <?php echo (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['name']; ?>">
                        <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="lastname">Lastname: <sup>*</sup></label>
                        <input type="text" name="lastname" class="form-control form-control-lg <?php echo (!empty($data['lastname_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['lastname']; ?>">
                        <span class="invalid-feedback"><?php echo $data['lastname_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="ZIP">ZIP: <sup>*</sup></label>
                        <input type="text" name="zip" class="form-control form-control-lg <?php echo (!empty($data['ZIP_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['ZIP']; ?>">
                        <span class="invalid-feedback"><?php echo $data['ZIP_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="city">City: <sup>*</sup></label>
                        <input type="text" name="city" class="form-control form-control-lg <?php echo (!empty($data['city_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['city']; ?>">
                        <span class="invalid-feedback"><?php echo $data['city_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="address">Address: <sup>*</sup></label>
                        <input type="text" name="address" class="form-control form-control-lg <?php echo (!empty($data['address_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['address']; ?>">
                        <span class="invalid-feedback"><?php echo $data['address_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone: <sup>*</sup></label>
                        <input type="text" name="phone" class="form-control form-control-lg <?php echo (!empty($data['phone_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['phone']; ?>">
                        <span class="invalid-feedback"><?php echo $data['phone_err']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="country">Country: <sup>*</sup></label>
                        <select name="country" class="form-control form-control-lg <?php echo (!empty($data['country_err'])) ? 'is-invalid' : ''; ?>" id = "country">
                            <option value="">Select Country</option>
                            <option value="Spain">Spain</option>
                            <option value="France">France</option>
                            <option value="Italy">Italy</option>
                            <option value="Germany">Germany</option>
                            <option value="United Kingdom">United Kingdom</option>
                            <option value="United States">United States</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="state">State: <sup>*</sup></label>
                        <select name="state" class="form-control form-control-lg <?php echo (!empty($data['state_err'])) ? 'is-invalid' : ''; ?>" id="state">
                            <option value="">Select State</option>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['state_err']; ?></span>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="hidden" name="NIF" value="<?php echo $data['NIF']; ?>">
                            <input type="submit" value="Edit" class="btn btn-success btn-block">
                        </div>
                        <div class="col">
                            <a href="<?php echo URLROOT; ?>/admin/index" class="btn btn-light btn-block">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var country = document.getElementById("country");
    var state = document.getElementById("state");

    country.addEventListener("change", function () {
        var country = document.getElementById("country").value;
        state.options.length = 0;
        if (country == "Spain") {
            var optionArray = ["|", "A Coruña|A Coruña", "Alava|Alava", "Albacete|Albacete", "Alicante|Alicante", "Almeria|Almeria", "Asturias|Asturias", "Avila|Avila", "Badajoz|Badajoz", "Baleares|Baleares", "Barcelona|Barcelona", "Burgos|Burgos", "Caceres|Caceres", "Cadiz|Cadiz", "Cantabria|Cantabria", "Castellon|Castellon", "Ceuta|Ceuta", "Ciudad Real|Ciudad Real", "Cordoba|Cordoba", "Cuenca|Cuenca", "Girona|Girona", "Granada|Granada", "Guadalajara|Guadalajara", "Guipuzcoa|Guipuzcoa", "Huelva|Huelva", "Huesca|Huesca", "Jaen|Jaen", "La Rioja|La Rioja", "Las Palmas|Las Palmas", "Leon|Leon", "Lleida|Lleida", "Lugo|Lugo", "Madrid|Madrid", "Malaga|Malaga", "Melilla|Melilla", "Murcia|Murcia", "Navarra|Navarra", "Ourense|Ourense", "Palencia|Palencia", "Pontevedra|Pontevedra", "Salamanca|Salamanca", "Santa Cruz de Tenerife|Santa Cruz de Tenerife", "Segovia|Segovia", "Sevilla|Sevilla", "Soria|Soria", "Tarragona|Tarragona", "Teruel|Teruel", "Toledo|Toledo", "Valencia|Valencia", "Valladolid|Valladolid", "Vizcaya|Vizcaya",
                "Zamora|Zamora", "Zaragoza|Zaragoza"];
        } else if (country == "France") {
            var optionArray = ["|", "Ain|Ain", "Aisne|Aisne", "Allier|Allier", "Alpes-de-Haute-Provence|Alpes-de-Haute-Provence", "Hautes-Alpes|Hautes-Alpes", "Alpes-Maritimes|Alpes-Maritimes", "Ardèche|Ardèche", "Ardennes|Ardennes", "Ariège|Ariège", "Aube|Aube", "Aude|Aude", "Aveyron|Aveyron", "Bouches-du-Rhône|Bouches-du-Rhône", "Calvados|Calvados", "Cantal|Cantal", "Charente|Charente", "Charente-Maritime|Charente-Maritime", "Cher|Cher", "Corrèze|Corrèze", "Corse-du-Sud|Corse-du-Sud", "Haute-Corse|Haute-Corse", "Côte-d'Or|Côte-d'Or", "Côtes-d'Armor|Côtes-d'Armor", "Creuse|Creuse", "Dordogne|Dordogne", "Doubs|Doubs", "Drôme|Drôme", "Eure|Eure", "Eure-et-Loir|Eure-et-Loir", "Finistère|Finistère", "Gard|Gard", "Haute-Garonne|Haute-Garonne", "Gers|Gers", "Gironde|Gironde", "Hérault|Hérault", "Ille-et-Vilaine|Ille-et-Vilaine", "Indre|Indre", "Indre-et-Loire|Indre-et-Loire", "Isère|Isère", "Jura|Jura", "Landes|Landes", "Loir-et-Cher|Loir-et-Cher", "Loire|Loire", "Haute-Loire|Haute-Loire", "Loire-Atlantique|Loire-Atlantique", "Loiret|Loiret", "Lot|Lot", "Lot-et-Garonne|Lot-et-Garonne", "Lozère|Lozère", "Maine-et-Loire|Maine-et-Loire", "Manche|Manche", "Marne|Marne", "Haute-Marne|Haute-Marne", "Mayenne|Mayenne", "Meurthe-et-Moselle|Meurthe-et-Moselle", "Meuse|Meuse", "Morbihan|Morbihan", "Moselle|Moselle", "Nièvre|Nièvre", "Nord|Nord", "Oise|Oise", "Orne|Orne", "Pas-de-Calais|Pas-de-Calais", "Puy-de-Dôme|Puy-de-Dôme", "Pyrénées-Atlantiques|Pyrénées-Atlantiques", "Hautes-Pyrénées|Hautes-Pyrénées", "Pyrénées-Orientales|Pyrénées-Orientales", "Bas-Rhin|Bas-Rhin", "Haut-Rhin|Haut-Rhin", "Rhône|Rhône", "Haute-Saône|Haute-Saône", "Saône-et-Loire|Saône-et-Loire", "Sarthe|Sarthe", "Savoie|Savoie", "Haute-Savoie|Haute-Savoie", "Paris|Paris", "Seine-Maritime|Seine-Maritime", "Seine-et-Marne|Seine-et-Marne", "Yvelines|Yvelines", "Deux-Sèvres|Deux-Sèvres", "Somme|Somme", "Tarn|Tarn", "Tarn-et-Garonne|Tarn-et-Garonne", "Var|Var", "Vaucluse|Vaucluse",
                "Vendée|Vendée", "Vienne|Vienne", "Haute-Vienne|Haute-Vienne", "Vosges|Vosges", "Yonne|Yonne", "Territoire de Belfort|Territoire de Belfort", "Essonne|Essonne", "Hauts-de-Seine|Hauts-de-Seine", "Seine-Saint-Denis|Seine-Saint-Denis", "Val-de-Marne|Val-de-Marne", "Val-d'Oise|Val-d'Oise"];
        } else if (country == "Germany") {
            var optionArray = ["|", "Baden-Württemberg|Baden-Württemberg", "Bayern|Bayern", "Berlin|Berlin", "Brandenburg|Brandenburg", "Bremen|Bremen", "Hamburg|Hamburg", "Hessen|Hessen", "Mecklenburg-Vorpommern|Mecklenburg-Vorpommern", "Niedersachsen|Niedersachsen", "Nordrhein-Westfalen|Nordrhein-Westfalen", "Rheinland-Pfalz|Rheinland-Pfalz", "Saarland|Saarland", "Sachsen|Sachsen", "Sachsen-Anhalt|Sachsen-Anhalt", "Schleswig-Holstein|Schleswig-Holstein", "Thüringen|Thüringen"];
        } else if (country == "Italy") {
            var optionArray = ["|", "Abruzzo|Abruzzo", "Basilicata|Basilicata", "Calabria|Calabria", "Campania|Campania", "Emilia-Romagna|Emilia-Romagna", "Friuli-Venezia Giulia|Friuli-Venezia Giulia", "Lazio|Lazio", "Liguria|Liguria", "Lombardia|Lombardia", "Marche|Marche", "Molise|Molise", "Piemonte|Piemonte", "Puglia|Puglia", "Sardegna|Sardegna", "Sicilia|Sicilia", "Toscana|Toscana", "Trentino-Alto Adige|Trentino-Alto Adige", "Umbria|Umbria", "Valle d'Aosta|Valle d'Aosta", "Veneto|Veneto"];
        } else if (country == "United States") {
            var optionArray = ["|", "Alabama|Alabama", "Alaska|Alaska", "Arizona|Arizona", "Arkansas|Arkansas", "California|California", "Colorado|Colorado", "Connecticut|Connecticut", "Delaware|Delaware", "District of Columbia|District of Columbia", "Florida|Florida", "Georgia|Georgia", "Hawaii|Hawaii", "Idaho|Idaho", "Illinois|Illinois", "Indiana|Indiana", "Iowa|Iowa", "Kansas|Kansas", "Kentucky|Kentucky", "Louisiana|Louisiana", "Maine|Maine", "Maryland|Maryland", "Massachusetts|Massachusetts", "Michigan|Michigan", "Minnesota|Minnesota", "Mississippi|Mississippi", "Missouri|Missouri", "Montana|Montana", "Nebraska|Nebraska", "Nevada|Nevada", "New Hampshire|New Hampshire", "New Jersey|New Jersey", "New Mexico|New Mexico", "New York|New York", "North Carolina|North Carolina", "North Dakota|North Dakota", "Ohio|Ohio", "Oklahoma|Oklahoma", "Oregon|Oregon", "Pennsylvania|Pennsylvania", "Rhode Island|Rhode Island", "South Carolina|South Carolina", "South Dakota|South Dakota", "Tennessee|Tennessee", "Texas|Texas", "Utah|Utah", "Vermont|Vermont", "Virginia|Virginia", "Washington|Washington", "West Virginia|West Virginia", "Wisconsin|Wisconsin", "Wyoming|Wyoming"];
        } else if (country == "United Kingdom") {
            var optionArray = ["|", "Aberdeenshire|Aberdeenshire", "Angus|Angus", "Argyllshire|Argyllshire", "Ayrshire|Ayrshire", "Banffshire|Banffshire", "Bedfordshire|Bedfordshire", "Berkshire|Berkshire", "Berwickshire|Berwickshire", "Buckinghamshire|Buckinghamshire", "Bute|Bute", "Caithness|Caithness", "Cambridgeshire|Cambridgeshire", "Cardiganshire|Cardiganshire", "Carmarthenshire|Carmarthenshire", "Cheshire|Cheshire", "Clackmannanshire|Clackmannanshire", "Cornwall|Cornwall", "Cromartyshire|Cromartyshire", "Cumberland|Cumberland", "Derbyshire|Derbyshire", "Devon|Devon", "Dorset|Dorset", "Dumfriesshire|Dumfriesshire", "Dunbartonshire|Dunbartonshire", "Durham|Durham", "East Lothian|East Lothian", "East Sussex|East Sussex", "Essex|Essex", "Fife|Fife", "Flintshire|Flintshire", "Glamorgan|Glamorgan", "Gloucestershire|Gloucestershire", "Greater Manchester|Greater Manchester", "Gwent|Gwent", "Gwynedd|Gwynedd", "Hampshire|Hampshire", "Herefordshire|Herefordshire", "Hertfordshire|Hertfordshire", "Highland|Highland", "Inverness-shire|Inverness-shire", "Isle of Wight|Isle of Wight", "Kent|Kent", "Lancashire|Lancashire", "Leicestershire|Leicestershire", "Lincolnshire|Lincolnshire", "Lothian|Lothian", "Merionethshire|Merionethshire", "Merseyside|Merseyside", "Mid Glamorgan|Mid Glamorgan", "Midlothian|Midlothian", "Monmouthshire|Monmouthshire", "Moray|Moray", "Neath Port Talbot|Neath Port Talbot", "Newport|Newport", "Norfolk|Norfolk", "North Yorkshire|North Yorkshire", "Northamptonshire|Northamptonshire", "Northumberland|Northumberland", "Nottinghamshire|Nottinghamshire", "Orkney|Orkney", "Oxfordshire|Oxfordshire", "Pembrokeshire|Pembrokeshire", "Perthshire|Perthshire", "Powys|Powys", "Renfrewshire|Renfrewshire", "Rhondda Cynon Taff", "Rutland|Rutland", "Scottish Borders|Scottish Borders", "Shetland|Shetland", "Shropshire|Shropshire", "Somerset|Somerset", "South Glamorgan|South Glamorgan", "South Yorkshire|South Yorkshire", "Staffordshire|Staffordshire", "Stirlingshire|Stirlingshire", "Suffolk|Suffolk", "Surrey|Surrey", "Sutherland|Sutherland", "Tyne and Wear|Tyne and Wear", "Tyrone|Tyrone", "Warwickshire|Warwickshire", "West Glamorgan|West Glamorgan", "West Lothian|West Lothian", "West Midlands|West Midlands", "West Sussex|West Sussex", "West Yorkshire|West Yorkshire", "Western Isles|Western Isles", "Wiltshire|Wiltshire", "Worcestershire|Worcestershire"];
        }
        for (var option in optionArray) {
            var pair = optionArray[option].split("|");
            var newOption = document.createElement("option");
            newOption.value = pair[0];
            newOption.innerHTML = pair[1];
            state.appendChild(newOption);
        }
        
    });


</script>
<?php include_once APPROOT . '/views/partials/adminfooter.php' ?>
<?php include_once APPROOT . "/views/partials/header.php"; ?>
<?php include_once APPROOT . "/views/partials/navbar2.php"; ?>

<div class="hero-wrap ftco-degree-bg" style="background-image: url(<?=URLROOT.'/images/bg_1.jpg'?>);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text justify-content-start align-items-center justify-content-center">
            <div class="col-lg-8 ftco-animate">
                <div class="text w-100 text-center mb-md-5 pb-md-5">
                    <h1 class="mb-4">CarRentingHouse</h1>
                    <p style="font-size: 18px;">
                        <strong>CarRentingHouse</strong> is a web application that allows you to rent a car online.
                        You can rent a car for a period of time, and you can also rent a car for a period of time.
                        You can also rent a car for a period of time.
                    </p>
                    <a href="https://www.youtube.com/watch?v=8fmgQMOynC4" class="icon-wrap popup-vimeo d-flex align-items-center mt-4 justify-content-center">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="ion-ios-play"></span>
                        </div>
                        <div class="heading-title ml-5">
                            <span>Easy steps for renting a car</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section ftco-no-pt bg-light">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-12	featured-top">
                <div class="row no-gutters">
                    <div class="col-md-4 d-flex align-items-center">
                        <form action="<?= URLROOT?>/users/loginUser" method="POST" class="request-form ftco-animate bg-primary">
                            <h2>Login</h2>
                            <div class="d-flex">
                                <div class="form-group">
                                    <label for="" class="label">Email</label>
                                    <input type="email" name="email" class="form-control <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['email']; ?>" placeholder="Email" required>
                                    <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="" class="label">NIF</label>
                                    <input type="text" name="NIF" class="form-control <?php echo (!empty($data['NIF_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['NIF']; ?>" placeholder="NIF" required>
                                    <span class="invalid-feedback"><?php echo $data['NIF_err']; ?></span>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-group mr-2">
                                    <label for="" class="label">Password</label>
                                    <input type="password"  class="form-control <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>" name="password" placeholder="Password" required>
                                    <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Login" class="btn btn-secondary py-3 px-4">
                            </div>
                            <div class="from-group">
                                <a href="<?=URLROOT?>/users/registerUser" class="btn btn-secondary py-3 px-4">Register</a>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8 d-flex align-items-center">
                        <div class="services-wrap rounded-right w-100">
                            <h3 class="heading-section mb-4">Better Way to Rent Your Perfect Cars</h3>
                            <div class="row d-flex mb-4">
                                <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                                    <div class="services w-100 text-center">
                                        <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-route"></span></div>
                                        <div class="text w-100">
                                            <h3 class="heading mb-2">Choose Your Pickup Location</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                                    <div class="services w-100 text-center">
                                        <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-handshake"></span></div>
                                        <div class="text w-100">
                                            <h3 class="heading mb-2">Select the Best Deal</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                                    <div class="services w-100 text-center">
                                        <div class="icon d-flex align-items-center justify-content-center"><span class="flaticon-rent"></span></div>
                                        <div class="text w-100">
                                            <h3 class="heading mb-2">Reserve Your Rental Car</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

<?php include_once APPROOT . "/views/partials/footer.php"; ?>
<?php
class File
{
    protected $file;
    protected $types;
    public function __construct($file, $types = ['image/jpeg', 'image/png', 'image/gif'])
    {
        $this->file = $file;
        $this->types = $types;
    }

    public function typeError()
    {
        $type = $this->file['type'];
        if (!in_array($type, $this->types)) {
            throw new FileException('El archivo no es una imagen');
        }
    }

    public function sizeError()
    {
        if (filesize($this->file) > ini_get('upload_max_filesize')) {
            throw new FileException('El archivo es demasiado grande. Tamano maximo: ' . ini_get('upload_max_filesize'));
        }
    }

    public function uploadError()
    {
        if (is_uploaded_file($this->file = $_FILES['image']['tmp_name']) === false) {
            throw new FileException('El archivo no se ha podido subir');
        }
    }

    public function moveError()
    {
        if (move_uploaded_file($this->file = $_FILES['image']['tmp_name'], 'img/' . $_FILES['image']['name']) === false) {
            throw new FileException('Ocurrió algún error al subir el fichero. No pudo guardarse');
        }
    }

    public function partialError()
    {
        if ($this->file['error'] !== UPLOAD_ERR_PARTIAL) {
            throw new FileException('El archivo no se ha podido subir');
        }
    }

    public function noFileError()
    {
        if ($this->file['error'] !== UPLOAD_ERR_NO_FILE) {
            throw new FileException('El archivo no se ha podido subir');
        }
    }

    public function noTmpDirError()
    {
        if ($this->file['error'] !== UPLOAD_ERR_NO_TMP_DIR) {
            throw new FileException('El archivo no se ha podido subir');
        }
    }

    public function PostSizeError()
    {
        if ($_SERVER['CONTENT_LENGTH'] > ini_get('post_max_size')) {
            throw new FileException('El archivo es demasiado grande. Tamano maximo: ' . ini_get('post_max_size'));
        }
    }
    
    public function checkErrors()
    {
        $this->PostSizeError();
        if ($this->file['error'] !== UPLOAD_ERR_OK) {


            $this->sizeError();

            $this->partialError();
            $this->noFileError();
            $this->noTmpDirError();
        }
        $this->typeError();
        $this->uploadError();
        $this->moveError();
    }
}

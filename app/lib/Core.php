<?php
class Core
{
    //Actual controller
    protected $currentController = 'Paginas';
    //Actual method
    protected $currentMethod = 'index';
    //Parameters
    protected $params = [];
    //Constructor
    public function __construct()
    {
        //print_r($this->getUrl());
        $url = $this->getUrl();
        //Look in controllers for first value
        if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) {
            //If exists, set as controller
            $this->currentController = ucwords($url[0]);
            //Unset 0 Index
            unset($url[0]);
        }
        //Require the controller
        require_once '../app/controllers/' . $this->currentController . '.php';
        //Instantiate controller class
        $this->currentController = new $this->currentController;
        //Check for second part of url
        if (isset($url[1])) {
            //Check to see if method exists in controller
            if (method_exists($this->currentController, $url[1])) {
                $this->currentMethod = $url[1];
                //Unset 1 index
                unset($url[1]);
            }
        }
        //Get params
        $this->params = $url ? array_values($url) : [];
        //Call a callback with array of params
        call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
    }

    //Get url
    public function getUrl()
    {
        //Get url
        $url = isset($_GET['url']) ? $_GET['url'] : 'Pages';
        //Remove slashes
        $url = rtrim($url, '/');
        //Remove query string
        $url = filter_var($url, FILTER_SANITIZE_URL);
        //Split url
        $url = explode('/', $url);

        return $url;
    }

    //get current controller
    public function getCurrentController()
    {
        return $this->currentController;
    }

    //set current controller
    public function setCurrentController($controller)
    {
        $this->currentController = $controller;
    }

    //get current method
    public function getCurrentMethod()
    {
        return $this->currentMethod;
    }

    //set current method
    public function setCurrentMethod($method)
    {
        $this->currentMethod = $method;
    }

    //get parameters
    public function getParams()
    {
        return $this->params;
    }
    //set parameters
    public function setParams($params)
    {
        $this->params = $params;
    }
}

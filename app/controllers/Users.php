<?php

use Tamtamchik\SimpleFlash\Flash;

class Users extends Controller
{

    public function __construct()
    {

        $this->userModel = $this->model('User');
        $this->rentsModel = $this->model('Rent');
    }

    // Admin add user
    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = [
                'title' => 'CarRentingHouse-Add-User',
                'name' => htmlentities($_POST['name']),
                'lastname' => htmlentities($_POST['last-name']),
                'email' => htmlentities($_POST['email']),
                'password' => htmlentities($_POST['password']),
                'confirm_password' => htmlentities($_POST['confirm-password']),
                'NIF' => htmlentities($_POST['NIF']),
                'ZIP' => htmlentities($_POST['zip']),
                'address' => htmlentities($_POST['address']),
                'city' => htmlentities($_POST['city']),
                'country' => htmlentities($_POST['country']),
                'phone' => htmlentities($_POST['phone']),
                'state' => htmlentities($_POST['state']),
                'admin' => 0,
                'name_err' => '',
                'lastname_err' => '',
                'email_err' => '',
                'password_err' => '',
                'NIF_err' => '',
                'ZIP_err' => '',
                'address_err' => '',
                'city_err' => '',
                'country_err' => '',
                'phone_err' => '',
                'state_err' => '',
                'confirm_password_err' => '',
                'role_err' => ''
            ];

            if (empty($data['name'])) {
                $data['name_err'] = 'Please enter name';
            }

            if (empty($data['lastname'])) {
                $data['lastname_err'] = 'Please enter lastname';
            }

            if (empty($data['email'])) {
                $data['email_err'] = 'Please enter email';
            }

            if (empty($data['password'])) {
                $data['password_err'] = 'Please enter password';
            }

            if (!empty($data['email']) && $this->userModel->findUserByEmail($data['email'])) {
                $data['email_err'] = 'Email is already taken';
            }

            if (!empty($data['NIF']) && !ValidateNIF($data['NIF'])) {
                $data['NIF_err'] = 'NIF is incorrect';
            }

            if (!empty($data['NIF']) && $this->userModel->getUserByNIF($data['NIF'])) {
                $data['NIF_err'] = 'NIF is already taken';
            }


            if (empty($data['NIF'])) {
                $data['NIF_err'] = 'Please enter NIF';
            }

            if (empty($data['ZIP'])) {
                $data['ZIP_err'] = 'Please enter ZIP';
            }

            if (empty($data['address'])) {
                $data['address_err'] = 'Please enter address';
            }

            if (empty($data['city'])) {
                $data['city_err'] = 'Please enter city';
            }

            if (empty($data['country'])) {
                $data['country_err'] = 'Please enter country';
            }

            if (empty($data['phone'])) {
                $data['phone_err'] = 'Please enter phone';
            }

            if (empty($data['state'])) {
                $data['state_err'] = 'Please enter state';
            }

            if (!empty($data['password']) && strlen($data['password']) < 6) {
                $data['password_err'] = 'Password must be at least 6 characters';
            }

            if (empty($data['confirm_password'])) {
                $data['confirm_password_err'] = 'Please confirm password';
            }

            if ($data['password'] != $data['confirm_password']) {
                $data['confirm_password_err'] = 'Passwords do not match';
            }

            if (empty($data['name_err']) && empty($data['lastname_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['NIF_err']) && empty($data['ZIP_err']) && empty($data['address_err']) && empty($data['city_err']) && empty($data['country_err']) && empty($data['phone_err']) && empty($data['state_err']) && empty($data['confirm_password_err'])) {
                $data['password'] = hash('sha512', $data['password']);
                $UserRegistered = $this->userModel->register($data);
                if ($UserRegistered) {
                    redirect('admin/index');
                } else {
                    die('Something went wrong');
                }
            } else {
                $this->view('users/add', $data);
            }
        } else {

            $data = [
                'title' => 'CarRentingHouse-Register',
                'name' => '',
                'lastname' => '',
                'email' => '',
                'password' => '',
                'NIF' => '',
                'ZIP' => '',
                'address' => '',
                'city' => '',
                'country' => '',
                'phone' => '',
                'state' => '',
                'admin' => '',
                'name_err' => '',
                'lastname_err' => '',
                'email_err' => '',
                'password_err' => '',
                'NIF_err' => '',
                'ZIP_err' => '',
                'address_err' => '',
                'city_err' => '',
                'country_err' => '',
                'phone_err' => '',
                'state_err' => '',
                'confirm_password_err' => '',
                'role_err' => ''
            ];

            $this->view('users/add', $data);
        }
    }
    // Delete user
    public function delete()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($this->userModel->deleteUser($_POST['NIF'])) {
                redirect('admin/index');
            } else {
                redirect('admin/index');
            }
        } else {
            redirect('admin/index');
        }
    }
    // Logout
    public function logout()
    {
        unset($_SESSION['user']);
        session_destroy();
        header('Location: ' . URLROOT);
    }
    //Login user
    public function loginUser()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $data = [
                'title' => 'CarRentingHouse-Login',
                'email' => htmlentities($_POST['email']),
                'password' => hash('sha512', htmlentities($_POST['password'])),
                'NIF' => htmlentities($_POST['NIF']),
                'email_err' => '',
                'password_err' => '',
                'NIF_err' => ''
            ];

            if (empty($data['email'])) {
                $data['email_err'] = 'Please enter email';
            }

            if (empty($data['password'])) {
                $data['password_err'] = 'Please enter password';
            }

            if (empty($data['NIF'])) {
                $data['NIF_err'] = 'Please enter NIF';
            }

            if (!empty($data['NIF']) && !ValidateNIF($data['NIF'])) {
                $data['NIF_err'] = 'NIF is incorrect';
            }

            if (empty($data['email_err']) && empty($data['password_err']) && empty($data['NIF_err'])) {
                $loggedInUser = $this->userModel->login($data['email'], $data['password'], $data['NIF']);

                if ($loggedInUser) {
                    // Create Session
                    $this->createUserSession($loggedInUser);
                } else {
                    $data['password_err'] = 'Password or email is incorrect';
                    $this->view('users/login', $data);
                }
            } else {
                $this->view('users/login', $data);
            }
        } else {
            $data = [
                'title' => 'CarRentingHouse-Login',
                'email' => '',
                'password' => '',
                'NIF' => '',
                'email_err' => '',
                'password_err' => '',
                'NIF_err' => ''
            ];
            $this->view('users/login', $data);
        }
    }
    //Register user
    public function registerUser()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = [
                'title' => 'CarRentingHouse-Register',
                'name' => htmlentities($_POST['name']),
                'lastname' => htmlentities($_POST['last-name']),
                'email' => htmlentities($_POST['email']),
                'password' => htmlentities($_POST['password']),
                'confirm_password' => htmlentities($_POST['confirm_password']),
                'NIF' => htmlentities($_POST['NIF']),
                'ZIP' => htmlentities($_POST['zip']),
                'address' => htmlentities($_POST['address']),
                'city' => htmlentities($_POST['city']),
                'country' => htmlentities($_POST['country']),
                'phone' => htmlentities($_POST['phone']),
                'state' => htmlentities($_POST['state']),
                'admin' => 0,
                'name_err' => '',
                'lastname_err' => '',
                'email_err' => '',
                'confirm_password_err' => '',
                'password_err' => '',
                'NIF_err' => '',
                'ZIP_err' => '',
                'address_err' => '',
                'city_err' => '',
                'country_err' => '',
                'phone_err' => '',
                'state_err' => '',
                'confirm_password_err' => ''
            ];

            if (empty($data['name'])) {
                $data['name_err'] = 'Please enter name';
            }

            if (empty($data['lastname'])) {
                $data['lastname_err'] = 'Please enter lastname';
            }

            if (empty($data['email'])) {
                $data['email_err'] = 'Please enter email';
            }

            if (empty($data['password'])) {
                $data['password_err'] = 'Please enter password';
            }

            if (!empty($data['email']) && $this->userModel->findUserByEmail($data['email'])) {
                $data['email_err'] = 'Email is already taken';
            }

            if (!empty($data['NIF']) && !ValidateNIF($data['NIF'])) {
                $data['NIF_err'] = 'NIF is incorrect';
            }

            if (!empty($data['NIF']) && $this->userModel->getUserByNIF($data['NIF'])) {
                $data['NIF_err'] = 'NIF is already taken';
            }


            if (empty($data['NIF'])) {
                $data['NIF_err'] = 'Please enter NIF';
            }

            if (empty($data['ZIP'])) {
                $data['ZIP_err'] = 'Please enter ZIP';
            }

            if (empty($data['address'])) {
                $data['address_err'] = 'Please enter address';
            }

            if (empty($data['city'])) {
                $data['city_err'] = 'Please enter city';
            }

            if (empty($data['country'])) {
                $data['country_err'] = 'Please enter country';
            }

            if (empty($data['phone'])) {
                $data['phone_err'] = 'Please enter phone';
            }
            

            if (empty($data['state'])) {
                $data['state_err'] = 'Please enter state';
            }

            if (!empty($data['password']) && strlen($data['password']) < 6) {
                $data['password_err'] = 'Password must be at least 6 characters';
            }

            if (empty($data['confirm_password'])) {
                $data['confirm_password_err'] = 'Please confirm password';
            }

            if ($data['password'] != $data['confirm_password']) {
                $data['confirm_password_err'] = 'Passwords do not match';
            }

            if (empty($data['name_err']) && empty($data['lastname_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['NIF_err']) && empty($data['ZIP_err']) && empty($data['address_err']) && empty($data['city_err']) && empty($data['country_err']) && empty($data['phone_err']) && empty($data['state_err']) && empty($data['confirm_password_err'])) {
                $data['password'] = hash('sha512', $data['password']);
                $UserRegistered = $this->userModel->register($data);
                if ($UserRegistered) {
                    $this->createUserSession($UserRegistered);
                } else {
                    die('Something went wrong');
                }
            } else {
                $this->view('users/register', $data);
            }
        } else {

            $data = [
                'title' => 'CarRentingHouse-Register',
                'name' => '',
                'lastname' => '',
                'email' => '',
                'password' => '',
                'NIF' => '',
                'ZIP' => '',
                'address' => '',
                'city' => '',
                'country' => '',
                'phone' => '',
                'state' => '',
                'admin' => 0,
                'name_err' => '',
                'lastname_err' => '',
                'email_err' => '',
                'password_err' => '',
                'NIF_err' => '',
                'ZIP_err' => '',
                'address_err' => '',
                'city_err' => '',
                'country_err' => '',
                'phone_err' => '',
                'state_err' => '',
                'confirm_password_err' => ''
            ];

            $this->view('users/register', $data);
        }
    }

    //Create User Session
    public function createUserSession($user)
    {
        $_SESSION['user'] = $user;
        header('Location: ' . URLROOT . '/paginas/index');
    }
    //Delete all users
    public function deleteAllUsers()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->userModel->deleteAllUsers();
            header('Location: ' . URLROOT . '/admin/index');
        }
    }
    //Confirm delete all users
    public function confirmDeleteAllUsers()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($_SESSION['user']->admin == 1) {


                $data = [
                    'title' => 'CarRentingHouse-DeleteAllUsers',
                ];

                $this->view('users/deleteall', $data);
            } else {
                redirect('admin/index');
            }
        } else {
            redirect('admin/index');
        }
    }
    //update user
    public function updateUser()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = [
                'title' => 'CarRentingHouse-UpdateUser',
                'name' => htmlentities($_POST['name']),
                'lastname' => htmlentities($_POST['lastname']),
                'ZIP' => htmlentities($_POST['ZIP']),
                'address' => htmlentities($_POST['address']),
                'city' => htmlentities($_POST['city']),
                'country' => htmlentities($_POST['country']),
                'phone' => htmlentities($_POST['phone']),
                'state' => htmlentities($_POST['state']),
                'NIF' => htmlentities($_POST['NIF']),
                'admin' => 0,
                'name_err' => '',
                'lastname_err' => '',
                'email_err' => '',
                'password_err' => '',
                'NIF_err' => '',
                'ZIP_err' => '',
                'address_err' => '',
                'city_err' => '',
                'country_err' => '',
                'phone_err' => '',
                'state_err' => '',
                'confirm_password_err' => ''
            ];

            if (empty($data['name'])) {
                $data['name_err'] = 'Please enter name';
            }

            if (empty($data['lastname'])) {
                $data['lastname_err'] = 'Please enter lastname';
            }

            if (empty($data['ZIP'])) {
                $data['ZIP_err'] = 'Please enter ZIP';
            }

            if (empty($data['address'])) {
                $data['address_err'] = 'Please enter address';
            }

            if (empty($data['city'])) {
                $data['city_err'] = 'Please enter city';
            }

            if (empty($data['country'])) {
                $data['country_err'] = 'Please enter country';
            }

            if (empty($data['phone'])) {
                $data['phone_err'] = 'Please enter phone';
            }

            if (empty($data['state'])) {
                $data['state_err'] = 'Please enter state';
            }


            if (empty($data['name_err']) && empty($data['lastname_err']) && empty($data['ZIP_err']) && empty($data['address_err']) && empty($data['city_err']) && empty($data['country_err']) && empty($data['phone_err']) && empty($data['state_err'])) {
                // dd($data);
                $usersUpdated =   $this->userModel->updateUser($data);
                if ($usersUpdated) {
                    header('Location: ' . URLROOT . '/admin/index');
                } else {
                    die('Something went wrong');
                }
            } else {
                $this->view('users/edit', $data);
            }
        } else {

            $user =  $this->userModel->getUserByNIF($_GET['NIF']);

            $data = [
                'title' => 'CarRentingHouse-UpdateUser',
                'name' => $user->name,
                'lastname' => $user->last_name,
                'ZIP' => $user->zip,
                'address' => $user->address,
                'city' => $user->City,
                'country' => $user->Country,
                'phone' => $user->phone,
                'state' => $user->State,
                'admin' => $user->admin,
                'NIF' => $user->NIF,
                'name_err' => '',
                'lastname_err' => '',
                'email_err' => '',
                'password_err' => '',
                'NIF_err' => '',
                'ZIP_err' => '',
                'address_err' => '',
                'city_err' => '',
                'country_err' => '',
                'phone_err' => '',
                'state_err' => '',
                'confirm_password_err' => ''
            ];

            $this->view('users/edit', $data);
        }
    }
    //Confirm delete user
    public function deleteUser()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($_SESSION['user']->admin == 1 && $this->userModel->getUserByNIF($_POST['NIF'])->admin == 0 && $_SESSION['user']->NIF != $_POST['NIF']) {

                if($this->rentsModel->findRentByUser($_POST['NIF'])){
                    redirect('admin/ierror');
                }
    

                $data = [
                    'title' => 'CarRentingHouse-DeleteUser',
                    'NIF' => $_POST['NIF'],
                ];

                $this->view('users/delete', $data);
            } else {
                redirect('admin/index');
            }
        } else {
            redirect('admin/index');
        }
    }
    //Confirm set admin user
    public function setadmin()
    {
        $data = [
            'title' => 'CarRentingHouse-SetAdmin',
            'NIF' => $_POST['NIF'],
        ];

        $this->view('users/setadmin', $data);
    }
    //set admin
    public function setadminUser()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->userModel->setAdmin($_POST['NIF']);
            header('Location: ' . URLROOT . '/admin/index');
        }
    }
}

<?php
class Rents extends Controller
{

    public function __construct()
    {
        $this->rentModel = $this->model('Rent');
        $this->userModel = $this->model('User');
        $this->carModel = $this->model('Car');
    }

    // User rents a car 
    public function make()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'users_NIF' => $_SESSION['user']->NIF,
                'Cars_Plate' => $_POST['Cars_Plate'],
                'Rent_Date' => Date('Y-m-d'),
                'Cars_err' => '',
                'users_err' => '',
            ];

            // Validate data
            if (empty($data['Cars_Plate'])) {
                $data['Cars_err'] = 'Please enter a plate number';
            }

            if ($this->carModel->findCarByPlate($data['Cars_Plate'])->status != 'Available') {
                $data['Cars_err'] = 'Car is not available';
            }


            // Make sure no errors
            if (empty($data['Cars_err'])) {
                // Validated
                if ($this->rentModel->addRent($data)) {
                    if ($this->carModel->updateCarStatus($data['Cars_Plate'], 'Borrowed')) {
                        redirect('paginas/cars');
                    }
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load view with errors
                $this->view('rents/make', $data);
            }
        } else {
            $data = [
                'title' => 'Make a rent',
                'user' => $_SESSION['user'],
                'Cars_Plate' => $_GET['Plate'],
                'Rent_Date' => date('Y-m-d H:i:s'),
                'Cars_err' => '',
                'users_err' => '',

            ];

            $this->view('rents/make', $data);
        }
    }
    // User returns a car (Update rent return date)
    public function return()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'Users_NIF' => $_SESSION['user']->NIF,
                'Cars_Plate' => $_POST['Cars_Plate'],
                'Cars_err' => '',
                'users_err' => '',
            ];

            // Validate data
            if (empty($data['Cars_Plate'])) {
                $data['Cars_err'] = 'Please enter a plate number';
            }

            if ($this->carModel->findCarByPlate($data['Cars_Plate'])->status != 'Borrowed') {
                $data['Cars_err'] = 'Car is not borrowed';
            }


            // Make sure no errors
            if (empty($data['Cars_err'])) {
                // Validated
                if ($this->rentModel->updateRent($data)) {
                    if ($this->carModel->updateCarStatus($data['Cars_Plate'], 'Available')) {
                        redirect('paginas/cars');
                    }
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load view with errors
                $this->view('rents/return', $data);
            }
        } else {
            $data = [
                'title' => 'Return a car',
                'user' => $_SESSION['user'],
                'Cars_Plate' => $_GET['Plate'],
                'Cars_err' => '',
                'users_err' => '',

            ];

            $this->view('rents/return', $data);
        }
    }
    // Admin returns a car (Update rent return date)
    public function returnadmin($NIF , $Plate)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'Users_NIF' => $_POST['Users_NIF'],
                'Cars_Plate' => $_POST['Cars_Plate'],
                'Cars_err' => '',
                'users_err' => '',
            ];

            // Validate data
            if (empty($data['Cars_Plate'])) {
                $data['Cars_err'] = 'Please enter a plate number';
            }

            if ($this->carModel->findCarByPlate($data['Cars_Plate'])->status != 'Borrowed') {
                $data['Cars_err'] = 'Car is not borrowed';
            }


            // Make sure no errors
            if (empty($data['Cars_err'])) {
                // Validated
                if ($this->rentModel->updateRent($data)) {
                    if ($this->carModel->updateCarStatus($data['Cars_Plate'], 'Available')) {
                        redirect('admin/index');
                    }
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load view with errors
                $this->view('rents/return', $data);
            }
        } else {
            $data = [
                'title' => 'Return a car',
                'user' => $NIF,
                'Cars_Plate' => $Plate,
                'Cars_err' => '',
                'users_err' => '',

            ];

            $this->view('rents/return', $data);
        }
    }
}

<?php

use \Tamtamchik\SimpleFlash\Flash;

class Cars extends Controller
{
    public function __construct()
    {
        $this->carsModel = $this->model('Car');
        $this->rentsModel = $this->model('Rent');
    }

    //Add car
    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'title' => 'Add Car',
                'name' => trim($_POST['name']),
                'price' => trim($_POST['price']),
                'description' => trim($_POST['description']),
                'image' => (!empty($_FILES['image']['name'])) ? $_FILES['image']['name'] : '',
                'status' => trim($_POST['status']),
                'luggage' => trim($_POST['luggage']),
                'fuel' => trim($_POST['fuel']),
                'seats' => trim($_POST['seats']),
                'Features' => json_encode($_POST['features']),
                'mileage' => trim($_POST['mileage']),
                'transmission' => trim($_POST['transmission']),
                'Brands' => $this->carsModel->getAllBrands(),
                'Brand_idBrands' => trim($_POST['Brand_idBrands']),
                'Plate' => trim($_POST['Plate']),
                'Years' => trim($_POST['Years']),
                'color' => trim($_POST['color']),
                'name_err' => '',
                'price_err' => '',
                'description_err' => '',
                'image_err' => '',
                'status_err' => '',
                'luggage_err' => '',
                'fuel_err' => '',
                'seats_err' => '',
                'Features_err' => '',
                'mileage_err' => '',
                'transmission_err' => '',
                'Brand_idBrands_err' => '',
                'Plate_err' => '',
                'Years_err' => '',
                'color_err' => ''
            ];
            // Validate data
            if (empty($data['name'])) {
                $data['name_err'] = 'Please enter name';
            }
            if (empty($data['price'])) {
                $data['price_err'] = 'Please enter price';
            }
            if (empty($data['description'])) {
                $data['description_err'] = 'Please enter description';
            }
            if (empty($data['status'])) {
                $data['status_err'] = 'Please enter status';
            }
            if (empty($data['luggage'])) {
                $data['luggage_err'] = 'Please enter luggage';
            }
            if (empty($data['fuel'])) {
                $data['fuel_err'] = 'Please enter fuel';
            }
            if (empty($data['seats'])) {
                $data['seats_err'] = 'Please enter seats';
            }
            if (empty($data['Features'])) {
                $data['Features_err'] = 'Please enter Features';
            }
            if (empty($data['mileage'])) {
                $data['mileage_err'] = 'Please enter mileage';
            }
            if (empty($data['transmission'])) {
                $data['transmission_err'] = 'Please enter transmission';
            }
            if (empty($data['Brand_idBrands'])) {
                $data['Brand_idBrands_err'] = 'Please enter Brand';
            }
            if (empty($data['Plate'])) {
                $data['Plate_err'] = 'Please enter Plate';
            }


            if (empty($data['Years'])) {
                $data['Years_err'] = 'Please enter Years';
            }
            if (empty($data['color'])) {
                $data['color_err'] = 'Please enter color';
            }

            if (!empty($_FILES['image'])) {

                try {
                    $file = new File($_FILES['image']);
                    $file->checkErrors();
                } catch (FileException $e) {
                    $data['image_err'] = $e->getMessage();
                }
            }
            // Make sure no errors
            if (empty($data['name_err']) && empty($data['mileage_err']) && empty($data['color_err']) && empty($data['price_err']) && empty($data['description_err']) && empty($data['status_err']) && empty($data['luggage_err']) && empty($data['fuel_err']) && empty($data['seats_err']) && empty($data['Features_err']) && empty($data['Brand_idBrands_err']) && empty($data['Plate_err']) && empty($data['Years_err']) && empty($data['transmission_err']) && empty($data['image_err'])) {
                // Validated
                if ($this->carsModel->addCar($data)) {
                    redirect('admin/index');
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load view with errors
                $this->view('cars/add', $data);
            }
        } else {
            $data = [
                'title' => 'Add Car',
                'name' => '',
                'price' => '',
                'description' => '',
                'image' => '',
                'status' => '',
                'luggage' => '',
                'fuel' => '',
                'seats' => '',
                'Features' => '',
                'mileage' => '',
                'transmission' => '',
                'Brands' => $this->carsModel->getAllBrands(),
                'Plate' => '',
                'Years' => '',
                'color' => '',
                'name_err' => '',
                'price_err' => '',
                'description_err' => '',
                'image_err' => '',
                'status_err' => '',
                'luggage_err' => '',
                'fuel_err' => '',
                'seats_err' => '',
                'Features_err' => '',
                'mileage_err' => '',
                'transmission_err' => '',
                'Brand_idBrands_err' => '',
                'Plate_err' => '',
                'Years_err' => '',
                'color_err' => ''
            ];

            $this->view('cars/add', $data);
        }
    }
    //Edit Car
    public function edit()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'name' => trim($_POST['name']),
                'price' => trim($_POST['price']),
                'description' => trim($_POST['description']),
                'image' => (!empty($_FILES['image']['name'])) ? $_FILES['image']['name'] : '',
                'status' => trim($_POST['status']),
                'luggage' => trim($_POST['luggage']),
                'fuel' => trim($_POST['fuel']),
                'seats' => trim($_POST['seats']),
                'mileage' => trim($_POST['mileage']),
                'transmission' => trim($_POST['transmission']),
                'Brand_idBrands' => trim($_POST['Brand_idBrands']),
                'Brands' => $this->carsModel->getAllBrands(),
                'Plate' => trim($_POST['Plate']),
                'Years' => trim($_POST['Years']),
                'color' => trim($_POST['color']),
                'name_err' => '',
                'price_err' => '',
                'description_err' => '',
                'status_err' => '',
                'luggage_err' => '',
                'fuel_err' => '',
                'seats_err' => '',
                'mileage_err' => '',
                'transmission_err' => '',
                'Brand_idBrands_err' => '',
                'Years_err' => '',
                'color_err' => ''
            ];

            // Validate data
            if (empty($data['name'])) {
                $data['name_err'] = 'Please enter name';
            }
            if (empty($data['price'])) {
                $data['price_err'] = 'Please enter price';
            }
            if (empty($data['description'])) {
                $data['description_err'] = 'Please enter description';
            }
            if (empty($data['status'])) {
                $data['status_err'] = 'Please enter status';
            }
            if (empty($data['luggage'])) {
                $data['luggage_err'] = 'Please enter luggage';
            }
            if (empty($data['fuel'])) {
                $data['fuel_err'] = 'Please enter fuel';
            }
            if (empty($data['seats'])) {
                $data['seats_err'] = 'Please enter seats';
            }
            if (empty($data['mileage'])) {
                $data['mileage_err'] = 'Please enter mileage';
            }
            if (empty($data['transmission'])) {
                $data['transmission_err'] = 'Please enter transmission';
            }
            if (empty($data['Brand_idBrands'])) {
                $data['Brand_idBrands_err'] = 'Please enter Brand';
            }
            if (empty($data['Years'])) {
                $data['Years_err'] = 'Please enter Years';
            }
            if (empty($data['color'])) {
                $data['color_err'] = 'Please enter color';
            }

            if (!empty($_FILES['image'])) {

                try {
                    $file = new File($_FILES['image']);
                    $file->checkErrors();
                } catch (FileException $e) {
                   $data['image_err'] = $e->getMessage();
                }
            }
            // Make sure no errors
            if (empty($data['name_err']) && empty($data['price_err']) && empty($data['description_err']) && empty($data['status_err']) && empty($data['luggage_err']) && empty($data['fuel_err']) && empty($data['seats_err']) && empty($data['mileage_err']) && empty($data['transmission_err']) && empty($data['Brand_idBrands_err']) && empty($data['Years_err']) && empty($data['color_err']) && empty($data['image_err'])) {
                // Validated
                if ($this->carsModel->updateCar($data)) {
                    flash('cars_message', 'Car updated');
                    redirect('admin/index');
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load view with errors
                $this->view('cars/edit', $data);
            }
        } else {
            // Get existing car from model
            $car = $this->carsModel->getCar($_GET['Plate']);
            
            $data = [
                'name' => $car->name,
                'price' => $car->price,
                'description' => $car->description,
                'image' => $car->image,
                'status' => $car->status,
                'luggage' => $car->luggage,
                'fuel' => $car->fuel,
                'seats' => $car->seats,
                'Features' => json_decode($car->Features),
                'mileage' => $car->mileage,
                'transmission' => $car->transmission,
                'Brands' => $this->carsModel->getAllBrands(),
                'Plate' => $car->Plate,
                'Years' => $car->Years,
                'color' => $car->color,
                'name_err' => '',
                'price_err' => '',
                'description_err' => '',
                'image_err' => '',
                'status_err' => '',
                'luggage_err' => '',
                'fuel_err' => '',
                'seats_err' => '',
                'Features_err' => '',
                'mileage_err' => '',
                'transmission_err' => '',
                'Brand_idBrands_err' => '',
                'Plate_err' => '',
                'Years_err' => '',
                'color_err' => ''
            ];
            

            $this->view('cars/edit', $data);
        }
    }
    // Delete car confirmation
    public function deleteCar()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Get existing car from model
            $car = $this->carsModel->getCar($_POST['Plate']);
            if ($this->carsModel->deleteCar($car->Plate)) {
                flash('cars_message', 'Car removed');
                redirect('admin/index');
            } else {
                die('Something went wrong');
            }
        } else {
            // Get existing car from model
            $car = $this->carsModel->getCar($_GET['Plate']);

            if($this->rentsModel->findRentByCar($car->Plate)){
                redirect('admin/ierror');
            }

            $data = [
                'Plate' => $car->Plate,
                'name' => $car->name
            ];
            $this->view('cars/delete', $data);
        }
    }
    //Add Brand
    public function addBrand()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'title' =>'Add Brand',
                'name' => trim($_POST['name']),
                'name_err' => '',
            ];

            // Validate data
            if (empty($data['name'])) {
                $data['name_err'] = 'Please enter name';
            }

            // Make sure no errors
            if (empty($data['name_err'])) {
                // Validated
                if ($this->carsModel->addBrand($data)) {
                    redirect('admin/index');
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load view with errors
                $this->view('cars/addbrand', $data);
            }
        } else {
            $data = [
                'title' => 'Add Brand',
                'name' => '',
                'name_err' => '',
            ];

            $this->view('cars/addbrand', $data);
        }
    }
    //Edit Brand
    public function updateBrand($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'id' => $id,
                'name' => trim($_POST['name']),
                'name_err' => '',
            ];

            // Validate data
            if (empty($data['name'])) {
                $data['name_err'] = 'Please enter name';
            }

            // Make sure no errors
            if (empty($data['name_err'])) {
                // Validated
                if ($this->carsModel->updateBrand($data)) {
                    flash('cars_message', 'Brand updated');
                    redirect('cars');
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load view with errors
                $this->view('cars/editBrand', $data);
            }
        } else {
            // Get existing car from model
            $brand = $this->carsModel->getBrand($id);

            // Check for owner
            if ($brand->user_id != $_SESSION['user_id']) {
                redirect('cars');
            }

            $data = [
                'id' => $id,
                'name' => $brand->name,
                'name_err' => '',
            ];

            $this->view('cars/editBrand', $data);
        }
    }
    //Delete Brand
    public function deleteBrand($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Get existing car from model
            $brand = $this->carsModel->getBrand($id);

            // Check for admin
            if (!IsAdmin()) {
                redirect('');
            }

            if ($this->carsModel->deleteBrand($brand->idBrands)) {
                flash('cars_message', 'Brand removed');
                redirect('cars');
            } else {
                die('Something went wrong');
            }
        } else {
            redirect('cars');
        }
    }
    //Prices Table
    public function pricesTable()
    {
        $data = [
            'cars' => $this->carsModel->getAllCars(),
            'brands' => $this->carsModel->getAllBrands()
        ];

        $this->view('cars/pricesTable', $data);
    }
    //Car Details
    public function carDetails()
    {
        $data = [
            'title' => 'Car Details',
            'car' => $this->carsModel->getCar($_GET['Plate']),
            'brands' => $this->carsModel->getAllBrands()
        ];

        $this->view('cars/details', $data);
    }
}

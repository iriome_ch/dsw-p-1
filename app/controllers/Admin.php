<?php

use \Tamtamchik\SimpleFlash\Flash;

class Admin extends Controller
{
    public function __construct()
    {

        $this->UserModel = $this->model('User');
        // $this->PostModel = $this->model('Post');
        // $this->CommentModel = $this->model('Comment');
        $this->CarModel = $this->model('Car');
        $this->RentModel = $this->model('Rent');
    }
    //Muestra el admin
    public function index()
    {
        if (!IsAdmin()) {
            Redirect('');
        } elseif (!IsLogged()) {
            redirect('user/login');
        } else {

            $data = [
                'title' => 'Admin Panel',
                'users' => $this->UserModel->getAllUsers(),
                'cars' => $this->CarModel->getAllCars(),
                'rents' => $this->RentModel->getRents(),
                // 'comments' => $this->CommentModel->getAllComments(),
                // 'posts' => $this->PostModel->getAllPosts(),
            ];

            $this->view('admin/index', $data);
        }
    }
    //Error de integridad
    public function ierror()
    {
        if (!IsAdmin()) {
            Redirect('');
        } elseif (!IsLogged()) {
            redirect('user/login');
        } else {
            $this->view('admin/i-error');
        }
    }
}

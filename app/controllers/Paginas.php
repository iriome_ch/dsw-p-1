<?php
//Page Controller
class Paginas extends Controller
{
    public function __construct()
    {
        $this->carsmodel = $this->model('Car');
        $this->usersmodel = $this->model('User');
        $this->rentmodel = $this->model('Rent');
        // $this->postsmodel = $this->model('Posts');
    }

    //Método para mostrar la página principal
    public function index()
    {
        $data = array(
            'title' => 'CarRentingHouse-Home',
            'cars' => $this->carsmodel->getManyFirstCars(6),
            'users' => $this->usersmodel->getManyFirstsUsers(15),
        );
        $this->view('paginas/index', $data);
    }

    //Método para mostrar la página de contacto
    public function contact()
    {
        $data = array(
            'title' => 'CarRentingHouse-Contact',
        );
        $this->view('paginas/contacto', $data);
    }

    //Método para mostrar la página de error 404
    public function error404()
    {
        $data = array(
            'title' => 'CarRentingHouse-Error404',
        );
        $this->view('paginas/error', $data);
    }

    //Metodo para mostrar la página de error 500
    public function error500()
    {
        $data = array(
            'title' => 'CarRentingHouse-Error500',
        );
        $this->view('paginas/error500', $data);
    }

    //Metodo para mostrar la pagina de about
    public function about()
    {
        $data = array(
            'title' => 'CarRentingHouse-About',
            'users'  => $this->usersmodel->getManyFirstsUsers(15),
            'UsersCount' => $this->usersmodel->countUsers(),
        );

        $this->view('paginas/about', $data);
    }

    //Metodo para mostrar la pagina de coches
    public function cars()
    {
        $data = array(
            'title' => 'CarRentingHouse-Cars',
            'cars' => $this->carsmodel->getAllCars(),
            'user'=> (isset($_SESSION['user'])) ? $_SESSION['user'] : null,
            'rents' => $this->rentmodel->getRents(),
        );

        $this->view('paginas/cars', $data);
    }
}
